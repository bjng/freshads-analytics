var assert = require('assert'),
	EventEmitter = require('events').EventEmitter,
	_ = require('lodash'),
	should = require('should');


var MongoClient = require('mongodb').MongoClient;


var AnalyticsStorage = require('./analytics').AnalyticsStorage;


var today = new Date();
today.setHours( 0,0,0,0 );
var eventsCount = 10000;


var getStorage = function( callback){
	MongoClient.connect( 'mongodb://localhost/analytics_test', function( err,db){
		if(err)
			callback(err,null)
		else callback( null, new AnalyticsStorage( db ) );
	});
};


function AEOIterator(count){
	this.count=count || 0;
	this._i = 0;
}

AEOIterator.prototype = {
	hasNext: function(){
		return this._i < this.count;
	},
	next: function(){
		if(this._i == this.count) return null;
		var aeo = { 'ts': Math.floor(today.getTime()/1000) ,'id' : String(this._i),'g':'','n':'touch','l':'','v':2 } 
		this._i++;
		return aeo;
	},
}


MongoClient.connect( 'mongodb://localhost/analytics_test',function(err,db){
	should.not.exist(err);
	should.exist(db);

	console.log('DB Connected');

	var storage = new AnalyticsStorage(db);

	storage.init( function(err,storage){
		should.not.exist(err);
		should.exist(storage);
		console.log('Storage Initialized');

		var start = new Date().getTime();
		function save( it,callback ){
			if( !it.hasNext() ){
				console.log( it.count,'events saved in', (new Date().getTime() - start ) / 1000,'sec' );
				callback(null,it.count)
				return;
			}
			storage.track( it.next(), function(err,result){

				should.not.exist(err);
				if(err){
					callback( err,null );
					return
				}			
				save(it,callback);

			});
		};

		console.log('Saving ', eventsCount, 'Test Events');
		
		save( new AEOIterator( eventsCount ), function(err, count){
			should.not.exist(err);
			count.should.be.exactly( eventsCount );


			function find( callback ){
				var start = new Date().getTime();
				storage.daily.find({'meta.ts':Math.floor( today.getTime()/1000 ) }).count( function(err, count){				
					callback(err,count);
					console.log('Events found in', (new Date().getTime() - start ) / 1000,'sec' );
				});
			};

			find( function(err,count){
				should.not.exist(err);
				count.should.be.exactly( eventsCount );

				storage.daily.findOne( {},function(err,doc ){
					console.log( 'Found example AEO',doc);
					db.dropDatabase(  function(err,result){

						should.not.exist( err );
						should.exist( result);
						console.log( 'Test DB has been dropped.');
						db.close();
					});
					
				});

			} );
		} );


	})

});
