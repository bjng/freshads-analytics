var _ = require('lodash');


PROPPREALLOCATE = 1.0/ 10000;
ID_DAILY = 'daily';

exports.fromQuery = function(query){
	return { 
		d: query.ts ? query.ts : Math.floor( new Date().getTime()/1000 ),
		uid: query.uid ? String(query.uid) : null,
		g: query.g ? String( query.g ) : '',
		n: query.n ? String(query.n) : null,
		l: query.l ? String(query.l) : null,
		v: query.v ? parseFloat( query.v ) : 0,
	}
}

exports.AnalyticsStorage = function( db ){
	this.db = db;	
	this.daily = this.db.collection( ID_DAILY );
}

exports.AnalyticsStorage.prototype = {
	init: function( callback ){
		var self = this;
		self.db.ensureIndex( ID_DAILY, {
			'uid':1,
			'g':1,
			'n':1,
			'l':1,
			'ts':1,
		}, function(err,result){
			if(!callback) return;
			if( err ) callback(err, null);
			else callback(null,self);
		}	);
	},
	track: function( aeo, callback ){
		
		var self = this;
		try{
			aeo = self.validate( _.clone(aeo) );
		}
		catch( err ){
			callback(err, 0 );
			return;
		}
		if( Math.random() * PROPPREALLOCATE  ){
			self._preallocate( aeo, function(err,count){
				self._updateDaily( aeo, callback );
			} );
		}
		else
			self._updateDaily( aeo, callback );
	},
	find: function( query,callback ){
		this.daily.find( query, function( err, cur ){
			if(callback)
				callback(err,cur)
		});
		
	},
	validate: function( aeo ){

		if(! aeo.uid )
			throw new Error( 'uid required')
		if(! aeo.n )
			throw new Error( 'name required')
		if(!aeo.ts )
			aeo.ts = Math.floor( new Date().getTime() / 1000 );

		aeo.uid = aeo.uid.replace(/^['"]|['"]$/gi,'');
		aeo.n = aeo.n.replace(/^['"]|['"]$/gi,'');
		aeo.g = (aeo.g || '').replace(/^['"]|['"]$/gi,'');
		aeo.l = (aeo.l || '').replace(/^['"]|['"]$/gi,'');
		aeo.v = parseFloat( aeo.v || 0 );

		return aeo;
	},
	/*
	nothing really to do in preallocation currently, 
	validation fills in all gaps and we don't go lower 
	than daily level or pre-aggregate monthly
	*/
	_preallocate: function( aeo, callback  ){
		callback( null, aeo )
		//this._updateDaily( aeo, callback  );
	},
	_updateDaily: function( aeo, callback ){
		var day = new Date( aeo.ts * 1000 );
		day.setHours(0,0,0,0);
		var dateId = [day.getUTCFullYear(),day.getUTCMonth(),day.getUTCDate()].join('');

		id_daily = [ dateId,aeo.uid, aeo.g, aeo.n,aeo.l].join('/');
		query = {
			'_id': id_daily,
			't': day, 
			'uid':aeo.uid, 
			'g':aeo.g, 
			'n':aeo.n,
			'l':aeo.l
		};
		update = { '$inc': { 'c':1, 'v' : aeo.v } };
		this.daily.update( query,update, {upsert:true,w:1},callback);
	},

}
