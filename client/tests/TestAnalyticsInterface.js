describe( "Analytics Interface", function(){

  var tracker;
  var now = new Date();
  console.log(' Tracker Id:', now.toUTCString() );
	beforeEach(function() {
    	
  	});

	 
	it("should by default exist globaly as 'fa'", function(){
  	expect( fa ).toBeDefined();
  });

  it("should change the tracking url via config cmd", function(){
     expect( function(){ fa('config', {trackingUrl: 'http://0.0.0.0:5109/track'}) } ).not.toThrow();
     expect( fa('config') ).not.toBeNull();
     expect( fa('config').trackingUrl ).toBeDefined();
     expect( fa('config').trackingUrl ).toEqual( 'http://0.0.0.0:5109/track' );
  });


  it("should throw an error an not return a tracker when no ID is defined", function(){
    expect( function(){ fa('create'); } ).toThrow();
  });

  it("should return a tracker if an Id was defined", function(){
   
    expect( function(){ tracker = fa('create', now.toUTCString() ) } ).not.toThrow();
    expect( tracker ).not.toBeNull();
    expect( tracker.id ).toEqual( now.toUTCString() );

  });


  it("Tracker should return local storage", function(){
      expect( tracker.storage() ).not.toBeNull();
  });

  it("Tracker should store an object with Id in storage", function(){
      expect( function(){ tracker._store( { id:"123" } ); } ).not.toThrow();
      expect( tracker.storage().events.length ).toEqual( 1 );
      expect( tracker.storage().events[0].id ).toEqual( "123" );
  });

  it("Tracker should send an AEO object to the server", function(){
      var aeo =  { 
        g: 'testGroup',
        n: 'ping' ,
        l: 'from test',
        v: 0,
      };

      expect( function(){ tracker.send( aeo.n, aeo.g, aeo.l, aeo.v ) } ).not.toThrow();
      
  });
});