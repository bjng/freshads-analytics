
( function(){
	
	var w = window;

	var key = w['FA_KEY'] || 'fa';
	
	if( w[ key ])
		return;

	var config = { 
		disabled:false,
		debug:true,
		trackingUrl:'http://track.fresh-ads.com/track'
	};

	w[ key ] = AnalyticsInterface;


	function AnalyticsInterface(  ){
		if(arguments.length == 0)
			return;
		var cmd = arguments[0];
		if(cmd == 'config'){
			if( !arguments[ 1 ])
				return config;
			try{
				config.debug = arguments[1]['debug'] || config.debug;
				config.disabled = arguments[1]['disabled'] || config.disabled;
				config.trackingUrl = arguments[1]['trackingUrl'] || config.trackingUrl;
			}
			catch(err){
				if(config.debug) throw err
			}

		}
		if( cmd == 'create'){
			var uid = arguments[1] || ''
			try{
				return registry.create( uid);
			}
			catch(err){
				throw err;
			}
		}
		else if( cmd.indexOf( 'send' ) != -1 ){
			var trackerMatch = cmd.match(/([^\.])\.(send)/gi);
			if( !trackerMatch && debug)
				throw Error('send command invalid')
			try{
				var args = arguments.slice(1)
				registry.getById( trackerMatch[1] ).send.call( args )
			}
			catch( err ){
				if( congfig.debug ) throw err;
			}
		}
	};

	function AnalyticsRegistry(){
		this.registry = { 'default':null };
	}

	AnalyticsRegistry.prototype={
		getById: function( uid ){
			return this.registry[ uid ] || null;
		},
		create: function( uid ){
			
			if( !uid )
				throw Error( 'ID for tracker not defined');
			if(!this.registry[ uid ]){
				this.registry[ uid ] = new AnalyticsTracker( uid, config.trackingUrl )
				if(!this.registry['default'])
					this.registry['default'] = this.registry[ uid ]
			}
			
			return this.registry[ uid ];
		}
	};

	function AnalyticsTracker( uid, url  ){
		if(!uid)
			throw Error('AnalyticsTracker id required')

		this.uid = uid;
		this.url = url || config.trackingUrl;
		this.storageId = ['fat',this.uid].join('_');
		this._sendStoredEvents();
		window.addEventListener('online',  this._sendStoredEvents );
  		window.addEventListener('offline', this._sendStoredEvents );
 
	}

	AnalyticsTracker.prototype = {
		isOnline:function(){
			return navigator.onLine;
		},
		send: function( name , group , label , value ){
			if(config.disabled) return;

			if(!this.url)
				throw Error('Url required')

			if(!name)
				throw Error('name required')

			var ts = Math.floor( new Date().getTime()/1000);
			var aeo = { ts:ts, uid:this.uid,g:group||'',n:name,l:label||'', v:value||'' };
			if(!this.isOnline())
					this._store( aeo );
			else
 				this._requestTrack( aeo );
		},
		storage: function( storage ){
			if(storage == undefined){
				var s = localStorage.getItem( this.storageId );
				return s ? JSON.parse( s ) : {}
			}
			else{
				localStorage.setItem(this.storageId, JSON.stringify(storage) );
			}
		},
		_requestTrack: function( aeo ){
			var imgRequest = new Image();
		    	imgRequest.onerror = function(){ imgRequest = null };
		    	imgRequest.onload = function(){ imgRequest = null };
		    	imgRequest.src = this.url + '?' + this._serialize( aeo );
		},
		_store: function( aeo ){
			var storage = this.storage();
			storage.events = storage.events || [];
			storage.events.push( aeo );
			this.storage( storage );
		},
		_sendStoredEvents:function( e ){
			if(!this.isOnline()) return;
			var storage = this.storage();
			if('events' in storage){
				var i = storage.length;
				while( i-- ){
					this.send( storage[i] );
				}
			}
		},
		_serialize: function( obj ){
			
			var str=[];
			for(var p in obj){
				if (obj.hasOwnProperty(p)) {
			   		if( obj[p] )
			   			str.push( encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			   }
			}
			return str.join("&");	
		}

	}


	var registry = new AnalyticsRegistry();

}());

