var cmd = require('commander'),
	analytics = require('./analytics'),
	http = require('http');

cmd
  .version('0.0.1')
  .option('-p, --port <n>', 'Specify port', parseInt)
  .option('-c, --config <path>', 'set config path. defaults to ./config.json')
  .parse(process.argv);

var port = cmd.port? cmd.port : 5109;

var config = require( cmd.config ? cmd.config  : './config.json');

var MongoClient = require('mongodb').MongoClient;

MongoClient.connect( config.mongoHost ,function(err,db){
	if(err)
		throw err
	
	var storage = new analytics.AnalyticsStorage( db );

	server = http.createServer(function (request, response) {
		var url = require('url');
		var url_parts = url.parse(request.url, true);

		if( url_parts.pathname == '/track' ){
			storage.track( analytics.fromQuery( url_parts.query ), function(err,result){
				if(err){
					throw err;
					response.writeHead(400 );
					response.end(err.message);
				}
				else{
					response.writeHead(200, {'Content-Type': 'image/gif'});
					response.end();
				}
			});
		}
		else if(url_parts.pathname == '/find' ){
			storage.find( url_parts.query, function( err, cursor ){
				if(err){
					throw err;
					response.writeHead(400 );
					response.end(err.message);
				}
				else{
					response.writeHead(200, {'Content-Type': 'application/json'});
					cursor.limit( 500 ).toArray( function( err, arr){
						response.end( JSON.stringify( arr ) );
					});
					
				}
			} );
		}
		else if(request.url.toLowerCase() == '/favicon.ico'){
			response.writeHead(404, {"Content-Type": "text/plain"});
  			response.write("404 Not Found\n");
			response.end();
			return;
		}
		else{
			response.writeHead(404, {"Content-Type": "text/plain"});
  			response.write("404 Not Found\n");
			response.end();	
		}


	}).listen(port, '0.0.0.0');

	console.log('Server running on 0.0.0.0:' + port);

});