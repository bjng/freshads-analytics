from fabric.api import *
from contextlib import contextmanager as _contextmanager

env.hosts = [ 'root@wsm-qi.com' ]
env.supervisor_name = 'freshadsanalytics'
env.project_root = '/home/analytics.fresh-ads.com'
env.git = 'git@git.wsm-qi.com:root/freshadsanalytics.git'


@_contextmanager
def virtualenv():
        with prefix(env.activate):
            yield

def develop():
	local("node app.js --config ./config.json");


def commit(message=None):
	with settings(warn_only=True):
		cmd = 'git commit -a';
		if message:
			cmd = cmd + " -m '" + message  + "'"
		local(cmd)

def push():
	local("git push")

def pull():
	local("git pull")

def pcp(message=None):
	pull() 
	commit( message )
	push()

def cpp(message=None):
	commit( message )
	pull()
	push()

def cp(message=None):
	commit( message )
	pull()

def compile():
	with settings(warn_only=True):
		local("java -jar client/compiler/compiler.jar --js=client/src/fa.js --js_output_file=client/dist/fa.js");		

def deploy():
	with settings(warn_only=True):
		if run("test -d %s" % env.project_root).failed:
			run("git clone %s %s" % (env.git, env.project_root ) )

	with cd(env.project_root):
		run("git pull")

def restart():
	run( 'supervisorctl restart ' + env.supervisor_name + ':')

def rebuild():
	run( "rm -f -R " + env.project_root)
	deploy()

def live():
	env.hosts = [ 'root@95.138.162.129' ]





