#Freshads - Analytics

Project for [Daily Mail UK](http://www.dailymail.co.uk/)

Daily Mail publishes a mobile version of their Daily Newspaper.
They needed a toolset that could deliver quickly Ads with some standard components like Image Galleries, Videos and 3D Turntables, that would be embedded into the daily Issue. It was supposed to be so easy to use that it could be used even by an Intern and also tracks reliable views and Interactions.

The full project consisted of following parts:

1. A sophisticated [Ad Builder](https://bitbucket.org/bjng/freshads-builder) based on HTML5 Canvas with a Python backend
2. Analytics Tracker Backend [Node.js Source](https://bitbucket.org/bjng/freshads-analytics/src) in particular the [Analytics Storage](https://bitbucket.org/bjng/freshads-analytics/src/9fa52a6f8a9cd90825c32f25f5ee479fd8cf09db/analytics.js?at=master&fileviewer=file-view-default)
3. Analytics clients script, [source](https://bitbucket.org/bjng/freshads-analytics/src/9fa52a6f8a9cd90825c32f25f5ee479fd8cf09db/client/src/fa.js?at=master&fileviewer=file-view-default)
4. A little in-house admin interface to view the Stats in Express.js [Analytics Admin](https://bitbucket.org/bjng/freshads-analytics-frontend)

---

This repo contains part 2. and 3.
Below some coding examples for these parts, for the other parts I refer to links above.

## Technologies
Node.js,Javascript, Mongo DB, [Fabric](http://www.fabfile.org/)

The Analytics tracker needed to handle a lot of user events, and we decided to create a really minimal service with no overload that can be easily extended, moved and scaled up if necessary.

Initially statistics where only aggregated per day, but it was planned to have the stats in down to an hourly resolution, in real time. To be ready for that we prepared the code following the recommendations of the Pre-Allocation Chapter of [Mongo Applied Design Patterns](https://library.oreilly.com/book/0636920027041/mongodb-applied-design-patterns/31.xhtml?ref=toc#_pre_allocate)


## Code Examples

Backend:

[Analytics Storage](https://bitbucket.org/bjng/freshads-analytics/src/9fa52a6f8a9cd90825c32f25f5ee479fd8cf09db/analytics.js?at=master&fileviewer=file-view-default)

Frontend:

[Client Script Source](https://bitbucket.org/bjng/freshads-analytics/src/9fa52a6f8a9cd90825c32f25f5ee479fd8cf09db/client/src/fa.js?at=master&fileviewer=file-view-default)

The client script was inspired by Googles Universal Analytics script, so that it was easy to use for most developers, since it pretty much followed the same syntax:

```
fa('create', AD_UID);
fa('send',name , group , label , value)
```

The script also made sure that events would be resend, in case a user wasn't online while seeing the Ad.



---

```
AEO - Analytics Event Object

d  = data
id = id 
g  = group
n  = name
l  = label
v  = value
t  = total

// eg:
{
	d: ISODate(...),
	id:"12345",
	g:"Image Component 1",
	n:"touch",
	l:"http://www.google.com",
	v:0,
}
```