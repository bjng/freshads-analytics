var _ = require('lodash'),
	AnalyticsStorage = require('./analytics').AnalyticsStorage,
	MongoClient = require('mongodb').MongoClient;



MongoClient.connect( 'mongodb://localhost/', function( err, client ){
	if(err) throw err;
	var olddb = client.db('freshads');

	var oldEvents = olddb.collection('analytics.events');

	var storage = new AnalyticsStorage( client.db('analytics') );
	storage.init();

	var count = 0;
	var failed = 0;

	var all = oldEvents.find();

	function patchNext( ){
		all.nextObject( function(err,doc){
			if(!doc){
				console.log( 'Patched',count,'Failed',failed );
				client.close();
				return;
			}

			var aeo =  {date: new Date( doc.date),
						id:doc.ad,
						group:doc.component || '',
						name:doc.name,
						label:doc.unique || '',
						value:doc.value
						};

			storage.track(aeo, function( err,result ){
				if(err)
					failed++;
				else
					count++;
				patchNext();
			});
		})
		
		

	}

	patchNext();

})
	

